﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;

namespace TicTAIToe.models
{
    public class Game
    {
        public GameState CurrentState { get; set; } = GameState.initiazation;
        public string ColorState { get; set; } = "Black";
        public string getDisplayState => this.GetDisplayState();

        public string gameDataVisibility  { get { return this.GameIsRunning() ? "Visible" : "Hidden"; } }

        public string finishDataString { get; set; }
        public string finishDataVisibility { get { return (this.finishDataString != null && !this.GameIsRunning()) ? "Visible" : "Hidden"; } }




        public Game()
        {
            this.CurrentState = GameState.running;
        }

        public bool GameIsRunning()
        {
            return this.CurrentState == GameState.running;
        }

        public string GetDisplayState()
        {
            switch(this.CurrentState)
            {
                case GameState.initiazation:
                    this.ColorState = Color.Purple.Name;
                    break;
                case GameState.running:
                    this.ColorState = Color.Green.Name;
                    break;
                case GameState.finished:
                    this.ColorState = Color.Red.Name;
                    break;
                default:
                    this.ColorState = Color.Black.Name;
                    break;
            }
            return this.CurrentState.ToString();

    }

    }
}
