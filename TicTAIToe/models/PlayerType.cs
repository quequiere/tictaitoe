﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTAIToe.models
{
    public enum PlayerType
    {
        one,
        two
    }

    public class PlayerTypeHelper
    {
        public static PlayerType inversePlayer(PlayerType player)
        {
            return player == PlayerType.one ? PlayerType.two : PlayerType.one;
        }

        public static PlayerType getTypeFromString(string type)
        {
            switch (type)
            {
                case "X":
                    return PlayerType.one;
                case "O":
                    return PlayerType.two;
                default:
                    throw new Exception($"Unknow typpe of player: {type}");
            }
        }
    }
}
