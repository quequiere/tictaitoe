﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TicTAIToe.events;

namespace TicTAIToe
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        public IGameService GameService { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindow(IGameService gameService) 
        {
            InitializeComponent();
            this.GameService = gameService;
          
            this.DataContext = this;
        }

    

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!(e.Source is TextBlock))
            {
                throw new Exception($"Error, unknow type of object: {e.Source.GetType()}");
            }


            TextBlock textBlock = e.Source as TextBlock;

            if(!int.TryParse(textBlock.Uid, out int id))
                throw new Exception($"Error while try to get clicked value {textBlock.Uid}");

            if(!this.GameService.TryTick(id))
                return;
            

            NotifyPropertyChanged("GameService");
            this.DataContext = this;

        }

        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Button_Reset_Click(object sender, RoutedEventArgs e)
        {
            this.GameService.resetGame();
            NotifyPropertyChanged("GameService");
            this.DataContext = this;
        }
    }
}
