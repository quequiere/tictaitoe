﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using TicTAIToe.models;

namespace TicTAIToe.events
{
    public class GameCalculator : IGameCalculator
    {
        private GameEventHandler GameEventListener;

        public GameCalculator(GameEventHandler gameEventListener)
        {
            this.GameEventListener = gameEventListener;
        }

        public PlayerType? tryCalculateLegitWin(string[] DatasGame)
        {
            bool gameFull = DatasGame.Count(c => c != null) == 9;
            var poentialWiner = tryFindGeometricSearch(DatasGame);

            if (poentialWiner != null)
            {
                return poentialWiner;
            }
            else if (poentialWiner == null && gameFull)
            {
                this.GameEventListener.FireGameFinishEvent(new GameFinishEventArgs(null, WinType.equality));
                return null;
            }
            return null;

        }


        private PlayerType? tryFindGeometricSearch(string[] DatasGame)
        {
            var geometricPossibilites = new List<string[]>();

            //Line possibilities
            geometricPossibilites.Add(new string[] { DatasGame[0], DatasGame[1], DatasGame[2]});
            geometricPossibilites.Add(new string[] { DatasGame[3], DatasGame[4], DatasGame[5]});
            geometricPossibilites.Add(new string[] { DatasGame[6], DatasGame[7], DatasGame[8]});

            //Vertical possibilities

            geometricPossibilites.Add(new string[] { DatasGame[0], DatasGame[3], DatasGame[6] });
            geometricPossibilites.Add(new string[] { DatasGame[1], DatasGame[4], DatasGame[7] });
            geometricPossibilites.Add(new string[] { DatasGame[2], DatasGame[5], DatasGame[8] });


            //diagonalPossibilities
            geometricPossibilites.Add(new string[] { DatasGame[0], DatasGame[4], DatasGame[8] });
            geometricPossibilites.Add(new string[] { DatasGame[2], DatasGame[4], DatasGame[6] });

            var alignement = geometricPossibilites.FirstOrDefault(it => matchWin(it));

            if (alignement == null)
                return null;

            return PlayerTypeHelper.getTypeFromString(alignement.First());

        }

        private bool matchWin(params string[] contents)
        {
            if (contents.Contains(null))
                return false;

            return contents.Distinct().Count()== 1;
        }
    }

    public interface IGameCalculator
    {
        public PlayerType? tryCalculateLegitWin(string[] DatasGame);
    }
}
