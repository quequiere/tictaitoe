﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using TicTAIToe.models;

namespace TicTAIToe.events
{
    public class GameService : IGameService
    {

        private GameEventHandler GameEventListener;
        private IGameCalculator GameCalculator;

        public Game Game { get; set; } = new Game();
        public string[] DatasGame { get; set; } = new string[9];

        public string getCurrentPlayer => this.getCurrentPlayerName();

        private PlayerType nextPlayer = PlayerType.one;

        public GameService(GameEventHandler gameEventListener, IGameCalculator gameCalculator)
        {
            this.GameEventListener = gameEventListener;
            this.GameCalculator = gameCalculator;
        }


        public bool TryTick( int location)
        {
            if (!this.Game.GameIsRunning())
                return false;
            

            if (!(location >= 0 && location <= 8))
            {
                this.looseGame(this.nextPlayer, WinType.actionError);
                return false;
            }
               

            if (DatasGame[location] != null)
            {
                this.looseGame(this.nextPlayer, WinType.actionError);

                return false;
            }

            this.doTick(nextPlayer, location);

            var potentialWiner = this.GameCalculator.tryCalculateLegitWin(DatasGame);

            if (potentialWiner.HasValue)
            {
                this.winGame(potentialWiner.Value);
            }

            this.nextPlayer = PlayerTypeHelper.inversePlayer(nextPlayer);
            return true;

        }

        private void doTick(PlayerType ptype, int location)
        {
            string toWrite = ptype == PlayerType.one ? "X" : "O";
            DatasGame[location] = toWrite;
        }

        public void looseGame(PlayerType looser, WinType type)
        {
            this.GameEventListener.FireGameFinishEvent(new GameFinishEventArgs(PlayerTypeHelper.inversePlayer(looser),type));
        }

        public void winGame(PlayerType type)
        {
            this.GameEventListener.FireGameFinishEvent(new GameFinishEventArgs(this.nextPlayer, WinType.logicalWin));

        }

        private string getCurrentPlayerName()
        {
            return this.nextPlayer == PlayerType.one ? "player one" : "player two";
        }

        public void resetGame()
        {
            Debug.WriteLine("Restarting game !");

            this.Game.CurrentState = GameState.initiazation;

            this.DatasGame = new string[9];
            this.nextPlayer = PlayerType.one;

            this.Game.CurrentState = GameState.running;
        }
    }

    public interface IGameService
    {
        public Game Game { get; set; }

        public string[] DatasGame { get; set; }

        public bool TryTick(int location);

        public void looseGame(PlayerType looser, WinType type);

        public string getCurrentPlayer { get; }

        public void resetGame();
    }

}
