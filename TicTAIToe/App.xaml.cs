﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TicTAIToe.events;

namespace TicTAIToe
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceProvider serviceProvider;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var services = new ServiceCollection();

            services.AddSingleton<GameEventHandler>();
            services.AddSingleton<GameEventListener>();
            services.AddSingleton<IGameCalculator,GameCalculator>();
            services.AddSingleton<IGameService , GameService>();
            services.AddSingleton<MainWindow>();

            serviceProvider = services.BuildServiceProvider();
            var mainWindow = serviceProvider.GetService<MainWindow>();

            //To force activation of dependecy injection
            serviceProvider.GetService<GameEventListener>();

            MainWindow.Show();
        }

    }

}
