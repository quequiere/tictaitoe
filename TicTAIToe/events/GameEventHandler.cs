﻿using System;
using System.Collections.Generic;
using System.Text;
using TicTAIToe.models;

namespace TicTAIToe.events
{
    public class GameEventHandler
    {
        public event EventHandler GameFinishHandler;

        public void FireGameFinishEvent(GameFinishEventArgs e)
        {
            EventHandler handler = GameFinishHandler;
            handler?.Invoke(this, e);
        }


    }

    public class GameFinishEventArgs : EventArgs
    {
        public PlayerType? Winner;
        public WinType WinType;

        public GameFinishEventArgs(PlayerType? winner, WinType winType)
        {
            this.Winner = winner;
            this.WinType = winType;
        }
    }
}
