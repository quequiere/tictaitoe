﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using TicTAIToe.models;

namespace TicTAIToe.events
{
    public class GameEventListener
    {
        private GameEventHandler GameEvent;
        private IGameService GameService;

        public GameEventListener(GameEventHandler gameEventListener, IGameService gameService)
        {
            this.GameEvent = gameEventListener;
            this.GameEvent.GameFinishHandler += onGameFinishEvent;
            this.GameService = gameService;
        }


        private void onGameFinishEvent(object sender, EventArgs e)
        {
            GameFinishEventArgs args = e as GameFinishEventArgs;
            this.GameService.Game.CurrentState = GameState.finished;

            switch(args.WinType)
            {
                case WinType.logicalWin:
                    this.GameService.Game.finishDataString = $"FINISHED player {args.Winner} win !";
                    break;
                case WinType.equality:
                    this.GameService.Game.finishDataString = $"FINISHED match equality :( !";
                    break;
                default:
                    this.GameService.Game.finishDataString = $"Error happened...";
                    break;
            }
                
            Debug.WriteLine($"Game finished, {args.Winner} player is victorious from {args.WinType} type.");
        }
    }
}
